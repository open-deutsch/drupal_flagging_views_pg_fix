# Open Deutsch Flagging Views PostgreSQL Fix

Fixes a problem of the Flag module with Views in Drupal when a PostgreSQL
database is in use.

More specifically, errors like the following are repaired:

    SQLSTATE[42883]: Undefined function: 7 ERROR: operator does not exist:
    integer = character varying
	LINE 6: ...lagging" "flagging_profile" ON profile.profile_id = flagging...

## Licence

Open Deutsch Flagging Views PostgreSQL Fix

Copyright 2019, 2023 Hans-Hermann Bode / Open Deutsch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2,
as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
